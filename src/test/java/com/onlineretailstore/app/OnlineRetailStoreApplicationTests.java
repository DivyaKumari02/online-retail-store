package com.onlineretailstore.app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OnlineRetailStoreApplicationTests {

	@InjectMocks
	OnlineRetailStoreApplication onlineRetailStoreApplication;

	@Test
	public void contextLoads() {
	}

	@Test
	public void main() {
		OnlineRetailStoreApplication.main(new String[] {});
	}

}

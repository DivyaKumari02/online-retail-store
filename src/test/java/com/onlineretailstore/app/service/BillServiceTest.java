package com.onlineretailstore.app.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.onlineretailstore.app.ErrorHandler.CustomException;
import com.onlineretailstore.app.dto.Bill;
import com.onlineretailstore.app.dto.Item;
import com.onlineretailstore.app.dto.Product;
import com.onlineretailstore.app.repository.BillRepository;
import com.onlineretailstore.app.repository.ItemRepository;
import com.onlineretailstore.app.repository.ProductRepository;
import com.onlineretailstore.app.rest.type.BillInfo;
import com.onlineretailstore.app.rest.type.ProductInfoForBill;
import com.onlineretailstore.app.util.ProductCategory;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BillServiceTest {

	@InjectMocks
	BillService billService;

	@Mock
	BillRepository billRepository;

	@Mock
	ItemRepository itemRepository;

	@Mock
	ProductRepository productRepository;

	@Mock
	Bill bill;

	@Mock
	BillInfo billInfo;
	
	@Mock
	Product product;
	
	@Mock 
	List<ProductInfoForBill> productsToBeAdded;
	
	@Mock 
	List<ProductInfoForBill> productsToBeRemoved;

	@Mock
	List<Item> item;
	
	@Mock
	List<Product> productsByBarCodeID;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testCreateBill() {
		Bill billtest = new Bill(item);
		billService.createBill(billtest);
	}

	@Test
	public void testDeleteBill() {
		Mockito.when(billRepository.findById(Mockito.anyLong())).thenReturn(null);
		billService.deleteBill(Mockito.anyLong());
	}

	@Test
	public void testDeleteBillSuccess() {
		Mockito.when(billRepository.findById(Mockito.anyLong())).thenReturn(bill);
		billService.deleteBill(Mockito.anyLong());
	}

	@Test
	public void testDeleteBillNotFound() {
		billService.deleteBill(null);
	}

	@Test
	public void testGetAllBills() {
		billService.getAllBills();
	}

	@Test
	public void testGetBillById() {
		billService.getBillById(Mockito.anyLong());
	}
	
	@Test
	public void testUpdateBillGetItemNull() {
		Item li = new Item();
		li.setQuantity(2);
		Mockito.when(billInfo.getProductsToBeAdded()).thenReturn(null);
		Mockito.when(billInfo.getProductsToBeRemoved()).thenReturn(null);
		Mockito.when(billRepository.findById(Mockito.anyLong())).thenReturn(bill);
		Mockito.when(bill.getItem()).thenReturn(null);
		billService.updateBill(billInfo, 3L);
	}
	
	@Test(expected = CustomException.class)
	public void testUpdateBillNull() {
		Mockito.when(billRepository.findById(Mockito.anyLong())).thenReturn(null);
		billService.updateBill(billInfo, Mockito.anyLong());
	}
	
	@Test(expected = CustomException.class)
	public void testUpdateBill() {
		BillInfo billInfoTest = new BillInfo();
		Bill billtest = new Bill(12, 30);
		billtest.setItem(item);
		Mockito.when(billService.createBill(billtest)).thenReturn(billtest);
		ProductInfoForBill productInfoForBillTest = new ProductInfoForBill();
		productInfoForBillTest.setBarCodeId("CODE-123-000");
		productInfoForBillTest.setQuantity(23);
		List<ProductInfoForBill> listOfProductInfoForBillTest = new ArrayList<>();
		listOfProductInfoForBillTest.add(productInfoForBillTest);
		billInfoTest.setProductsToBeAdded(listOfProductInfoForBillTest);
		billInfoTest.setProductsToBeRemoved(listOfProductInfoForBillTest);
		Mockito.when(productRepository.findByBarCodeId("CODE-123-000")).thenReturn(productsByBarCodeID);
		Mockito.when(bill.getItem()).thenReturn(item);
		Mockito.when(billRepository.findById(Mockito.anyLong())).thenReturn(bill);
		billService.updateBill(billInfoTest, Mockito.anyLong());
	}
	
	@Test(expected = NullPointerException.class)
	public void testUpdateBillCurrentItem() {
		Product p = new Product();
		p.setBarCodeId("CODE-123-000");
		Item li1 = new Item(p, 10);
		Item li2 = new Item(p, 15);
		List<Item> currentItems = new ArrayList<>();
		currentItems.add(li1);
		currentItems.add(li2);
		BillInfo billInfoTest = new BillInfo();
		Bill billtest = new Bill(12, 30);
		billtest.setItem(item);
		Mockito.when(billService.createBill(billtest)).thenReturn(billtest);
		ProductInfoForBill productInfoForBillTest = new ProductInfoForBill("CODE-123-000", 23);
		List<ProductInfoForBill> listOfProductInfoForBillTest = new ArrayList<>();
		listOfProductInfoForBillTest.add(productInfoForBillTest);
		billInfoTest.setProductsToBeAdded(listOfProductInfoForBillTest);
		billInfoTest.setProductsToBeRemoved(listOfProductInfoForBillTest);
		Mockito.when(productRepository.findByBarCodeId("CODE-123-000")).thenReturn(productsByBarCodeID);
		Mockito.when(bill.getItem()).thenReturn(currentItems);
		Mockito.when(billRepository.findById(Mockito.anyLong())).thenReturn(bill);
		billService.updateBill(billInfoTest, Mockito.anyLong());
	}
	
	@Test(expected = CustomException.class)
	public void testUpdateBillCurrentItemNull() {
		BillInfo billInfoTest = new BillInfo();
		Bill billtest = new Bill(12, 30);
		billtest.setItem(item);
		Mockito.when(billService.createBill(billtest)).thenReturn(billtest);
		ProductInfoForBill productInfoForBillTest = new ProductInfoForBill();
		productInfoForBillTest.setBarCodeId("CODE-123-000");
		productInfoForBillTest.setQuantity(23);
		List<ProductInfoForBill> listOfProductInfoForBillTest = new ArrayList<>();
		listOfProductInfoForBillTest.add(productInfoForBillTest);
		billInfoTest.setProductsToBeAdded(listOfProductInfoForBillTest);
		billInfoTest.setProductsToBeRemoved(listOfProductInfoForBillTest);
		Mockito.when(productRepository.findByBarCodeId("CODE-123-000")).thenReturn(productsByBarCodeID);
		Mockito.when(bill.getItem()).thenReturn(null);
		Mockito.when(billRepository.findById(Mockito.anyLong())).thenReturn(bill);
		billService.updateBill(billInfoTest, Mockito.anyLong());
	}
	
	@Test(expected = CustomException.class)
	public void testUpdateBillBarCodeNull() {
		BillInfo billInfoTest = new BillInfo();
		Bill billtest = new Bill(12, 30);
		billtest.setItem(item);
		Mockito.when(billService.createBill(billtest)).thenReturn(billtest);
		ProductInfoForBill productInfoForBillTest = new ProductInfoForBill();
		productInfoForBillTest.setBarCodeId("CODE-123-000");
		productInfoForBillTest.setQuantity(23);
		List<ProductInfoForBill> listOfProductInfoForBillTest = new ArrayList<>();
		listOfProductInfoForBillTest.add(productInfoForBillTest);
		billInfoTest.setProductsToBeAdded(listOfProductInfoForBillTest);
		billInfoTest.setProductsToBeRemoved(listOfProductInfoForBillTest);
		Mockito.when(billRepository.findById(Mockito.anyLong())).thenReturn(bill);
		billService.updateBill(billInfoTest, Mockito.anyLong());
	}

	@Test(expected = NullPointerException.class)
	public void testUpdateBillProductCatA() {
		Product p1 = new Product();
		Item li1 = new Item(product, 3);
		Item li2 = new Item(product, 5);
		p1.setProductCategory(ProductCategory.A);
		li1.setProduct(p1);
		List<Item> listitems = new ArrayList<>();
		listitems.add(li1);
		listitems.add(li2);
		Mockito.when(billInfo.getProductsToBeAdded()).thenReturn(null);
		Mockito.when(billInfo.getProductsToBeRemoved()).thenReturn(null);
		Mockito.when(billRepository.findById(Mockito.anyLong())).thenReturn(bill);
		Mockito.when(bill.getItem()).thenReturn(listitems);
		billService.updateBill(billInfo, 3L);
	}
	
	@Test(expected = NullPointerException.class)
	public void testUpdateBillProductCatB() {
		Product p1 = new Product();
		Item li1 = new Item(product, 3);
		Item li2 = new Item(product, 5);
		p1.setProductCategory(ProductCategory.B);
		li1.setProduct(p1);
		List<Item> listitems = new ArrayList<>();
		listitems.add(li1);
		listitems.add(li2);
		Mockito.when(billInfo.getProductsToBeAdded()).thenReturn(null);
		Mockito.when(billInfo.getProductsToBeRemoved()).thenReturn(null);
		Mockito.when(billRepository.findById(Mockito.anyLong())).thenReturn(bill);
		Mockito.when(bill.getItem()).thenReturn(listitems);
		billService.updateBill(billInfo, 3L);
	}
	
	@Test(expected = NullPointerException.class)
	public void testUpdateBillProductCatC() {
		Product p1 = new Product();
		Item li1 = new Item(product, 3);
		Item li2 = new Item(product, 5);
		p1.setProductCategory(ProductCategory.C);
		li1.setProduct(p1);
		List<Item> listitems = new ArrayList<>();
		listitems.add(li1);
		listitems.add(li2);
		Mockito.when(billInfo.getProductsToBeAdded()).thenReturn(null);
		Mockito.when(billInfo.getProductsToBeRemoved()).thenReturn(null);
		Mockito.when(billRepository.findById(Mockito.anyLong())).thenReturn(bill);
		Mockito.when(bill.getItem()).thenReturn(listitems);
		billService.updateBill(billInfo, 3L);
	}

	@Test(expected = CustomException.class)
	public void testUpdateBillCustomException() {
		Mockito.when(billInfo.getProductsToBeAdded()).thenReturn(null);
		Mockito.when(billInfo.getProductsToBeRemoved()).thenReturn(null);
		billService.updateBill(null, 0L);
	}

}

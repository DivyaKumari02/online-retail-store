package com.onlineretailstore.app.service;

import static org.junit.Assert.fail;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.onlineretailstore.app.dto.Product;
import com.onlineretailstore.app.repository.ProductRepository;
import com.onlineretailstore.app.rest.type.ProductData;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {
	
	@InjectMocks
	ProductService productService;
	
	@Mock
	ProductRepository productRepository;
	
	@Mock
	List<Product> productList;
	
	@Mock
	Product product;
	
	@Mock
	ProductData productData;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testGetAllProduct() {
		productService.getAllProduct();
	}
	
	@Test
	public void testGetProductById() {
		productService.getProductById(Mockito.anyLong());
	}

	@Test
	public void testCreateProduct() {
		productService.createProduct(productData);
	}
	
	@Test
	public void testCreateProductNotCreated() {
		productService.createProduct(null);
	}

	@Test
	public void testUpdateProduct() {
		ProductData productData = new ProductData();
		productData.setName("Pears");
		Mockito.when(productService.getProductById(Mockito.anyLong())).thenReturn(product);
		Mockito.when(productService.updateProduct(2L, productData)).thenReturn(product);
		productService.updateProduct(3L, productData);
	}
	
	@Test
	public void testUpdateProductProductNotFound() {
		Mockito.when(productService.updateProduct(2L, productData)).thenReturn(product);
		productService.updateProduct(3L, productData);
	}

	@Test
	public void testDeleteProduct() {
		productService.deleteProduct(Mockito.anyLong());
	}
	
	@Test
	public void testDeleteProductSuccess() {
		Mockito.when(productService.getProductById(Mockito.anyLong())).thenReturn(product);
		productService.deleteProduct(Mockito.anyLong());
	}
	
	@Test
	public void testDeleteProductIdNull() {
		productService.deleteProduct(null);
	}

}

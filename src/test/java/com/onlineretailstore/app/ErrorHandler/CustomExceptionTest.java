package com.onlineretailstore.app.ErrorHandler;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomExceptionTest {
	
	@InjectMocks
	CustomException customException;
	
	String message = "Error message";
	
	@Mock
	Throwable cause;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() {
		customException = new CustomException();
	}
	
	@Test
	public void testCustomExceptionWithMessage() {
		customException = new CustomException(message);
	}
	
	@Test
	public void testCustomExceptionWithMessageAndThrowable() {
		customException = new CustomException(message, cause);
	}

}

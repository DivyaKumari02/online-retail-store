package com.onlineretailstore.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.onlineretailstore.app.config.RestGenericTemplate;
import com.onlineretailstore.app.config.RestGenericTemplateSingle;
import com.onlineretailstore.app.dto.Bill;
import com.onlineretailstore.app.rest.type.BillInfo;
import com.onlineretailstore.app.rest.type.ProductInfoForBill;
import com.onlineretailstore.app.service.BillService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BillControllerTest {
	
	@InjectMocks
	BillController billController;
	
	@Mock
	BillService billService;
	
	@Mock
	BillInfo billInfo;
	
	@Mock
	List<Bill> billList;
	
	@Mock
	Bill bill;
	
	@Mock
	ProductInfoForBill productInfoForBill;
	
	@Mock
	List<ProductInfoForBill> productsToBeAdded;
	
	@Mock
	RestGenericTemplate<Bill> responseBill;
	
	@Mock
	RestGenericTemplateSingle<Bill> singleResponseBill;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void getAllBillsTest() {
		Bill bill = new Bill();
		bill.setNoOfItems(10);
		Mockito.when(billService.getAllBills()).thenReturn(billList);
		billController.getAllBills();
	}
	
	@Test
	public void getAllBillsErrorTest() {
		billController.getAllBills();
	}
	
	@Test
	public void getBillByIdTest() {
		Mockito.when(billService.getBillById(Mockito.anyLong())).thenReturn(bill);
		billController.getBillById(Mockito.anyLong());
	}
	
	@Test
	public void getBillByIdNotFoundTest() {
		billController.getBillById(Mockito.anyLong());
	}
	
	@Test
	public void createBillTest() {
//		Bill bill1 = new Bill();
//		bill1.setNoOfItems(2);
		Bill billcreate = new Bill(0.0, 0);
		Mockito.when(billService.createBill(Mockito.any())).thenReturn(billcreate);
		billController.createBill();
	}
	
	@Test
	public void createBillNotCreatedTest() {
		billController.createBill();
	}
	
	@Test
	public void deleteBillTest() {
		Mockito.when(billService.deleteBill(8L)).thenReturn("success");
		billController.deleteBill(Mockito.anyLong());
	}
	
	@Test
	public void deleteBillNotDeletedTest() {
		billController.deleteBill(Mockito.anyLong());
	}
	
	@Test
	public void updateBillTest() {
//		Bill billcreate = new Bill(0.0, 0);
//		BillInfo billInfoUpdate = new BillInfo();
//		List<ProductInfoForBill> productsList = new ArrayList<ProductInfoForBill>();
//		ProductInfoForBill productInfoForBill = new ProductInfoForBill();
//		productInfoForBill.setQuantity(10);
//		productsList.add(productInfoForBill);
//		billInfoUpdate.setProductsToBeAdded(productsList);
//		billInfoUpdate.setProductsToBeRemoved(productsList);
		Mockito.when(billService.updateBill(billInfo, 7L)).thenReturn(bill);
		billController.updateBill(billInfo, 9L);
	}
	
	@Test
	public void updateBillNotUpdatedTest() {
		billController.updateBill(billInfo, 9L);
	}
}

package com.onlineretailstore.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.onlineretailstore.app.config.RestGenericTemplate;
import com.onlineretailstore.app.dto.Product;
import com.onlineretailstore.app.rest.type.ProductData;
import com.onlineretailstore.app.service.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductsControllerTest {
	
	@InjectMocks
	ProductsController productsController;
	
	@Mock
	ProductService productService;
	
	@Mock
	ProductData productData;
	
	@Mock
	RestGenericTemplate<Product> responseProduct;
	
	@Mock
	List<Product> productList;
	
	@Mock
	Product product;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void findProductsTest() {
		List<Product> productList = new ArrayList<>();
		Product pr1 = new Product();
		pr1.setName("Dove");
		productList.add(pr1);
		Mockito.when(productService.getAllProduct()).thenReturn(productList);
		productsController.findProducts();
	}
	
	@Test
	public void findProductsElseTest() {
		productsController.findProducts();
	}
	
	@Test
	public void findProductByIdTest() {
		Mockito.when(productService.getProductById(Mockito.anyLong())).thenReturn(product);
		productsController.findProductById(Mockito.anyLong());
	}
	
	@Test
	public void findProductByIdNotFoundTest() {
		productsController.findProductById(Mockito.anyLong());
	}
	
	@Test
	public void createProductsTest() {
		Mockito.when(productService.createProduct(productData)).thenReturn(product);
		productsController.createProduct(productData);
	}
	
	@Test
	public void createProductsNotCreatedTest() {
		productsController.createProduct(productData);
	}
	
	
	@Test
	public void updateProductsTest() {
		Mockito.when(productService.updateProduct(5L, productData)).thenReturn(product);
		productsController.updateProduct(5L, productData);
	}
	
	@Test
	public void updateProductsNotUpdatedTest() {
		productsController.updateProduct(4L, productData);
	}
	
	@Test
	public void deleteProductsTest() {
		Mockito.when(productService.deleteProduct(Mockito.anyLong())).thenReturn("success");
		productsController.deleteProduct(Mockito.anyLong());
	}
	
	@Test
	public void deleteProductsNotDeletedTest() {
		productsController.deleteProduct(Mockito.anyLong());
	}

}

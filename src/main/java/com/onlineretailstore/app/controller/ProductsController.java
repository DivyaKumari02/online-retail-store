package com.onlineretailstore.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onlineretailstore.app.config.RestGenericTemplate;
import com.onlineretailstore.app.config.RestGenericTemplateSingle;
import com.onlineretailstore.app.dto.Product;
import com.onlineretailstore.app.rest.type.ProductData;
import com.onlineretailstore.app.service.ProductService;
import com.onlineretailstore.app.util.Constant;

@RestController
@RequestMapping("/retail-store")
public class ProductsController {
	
	@Autowired
	ProductService productService; 
	
	@GetMapping(value="/products", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RestGenericTemplate<Product>> findProducts() {
		final RestGenericTemplate<Product> response = new RestGenericTemplate<>();
		List<Product> product = productService.getAllProduct();
		if(!CollectionUtils.isEmpty(product)) {
			response.setData(product);
			response.setTotalRecords(product.size());
			response.setResponseCode(Constant.SUCCESS_CODE);
			response.setMessage("success");
		}
		return new ResponseEntity<RestGenericTemplate<Product>>(response, HttpStatus.OK);
	}
	
	@GetMapping(value="/products/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RestGenericTemplateSingle<Product>> findProductById(@PathVariable("id") Long id) {
		final RestGenericTemplateSingle<Product> response = new RestGenericTemplateSingle<>();
		Product product = productService.getProductById(id);
		if(product != null) {
			response.setData(product);
			response.setResponseCode(Constant.SUCCESS_CODE);
			response.setMessage("success");
		}
		return new ResponseEntity<RestGenericTemplateSingle<Product>>(response, HttpStatus.OK);
	}
	
	@PostMapping(value="/products", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RestGenericTemplateSingle<Product>> createProduct(@RequestBody ProductData productData) {
		final RestGenericTemplateSingle<Product> result = new RestGenericTemplateSingle<Product>();
		Product product = productService.createProduct(productData);
		if(product != null) {
			result.setData(product);
			result.setResponseCode(Constant.SUCCESS_CODE);
			result.setMessage("success");
		}
		return new ResponseEntity<RestGenericTemplateSingle<Product>>(result, HttpStatus.OK);
	}
	
	@PutMapping(value="/products/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RestGenericTemplateSingle<Product>> updateProduct(@PathVariable("id") Long id, @RequestBody ProductData productData) {
		final RestGenericTemplateSingle<Product> result = new RestGenericTemplateSingle<Product>();
		Product product = productService.updateProduct(id, productData);
		if(product!=null) {
			result.setData(product);
			result.setResponseCode(Constant.SUCCESS_CODE);
			result.setMessage("success");
		}
		return new ResponseEntity<RestGenericTemplateSingle<Product>>(result, HttpStatus.OK);
	}
	
	@DeleteMapping(value="/products/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RestGenericTemplateSingle<String>> deleteProduct(@PathVariable("id") Long id) {
		final RestGenericTemplateSingle<String> stringResult = new RestGenericTemplateSingle<String>();
		stringResult.setMessage(productService.deleteProduct(id));
		return new ResponseEntity<RestGenericTemplateSingle<String>>(stringResult, HttpStatus.OK);
	}
	

}

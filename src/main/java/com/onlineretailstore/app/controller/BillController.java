package com.onlineretailstore.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onlineretailstore.app.config.RestGenericTemplate;
import com.onlineretailstore.app.config.RestGenericTemplateSingle;
import com.onlineretailstore.app.dto.Bill;
import com.onlineretailstore.app.rest.type.BillInfo;
import com.onlineretailstore.app.service.BillService;
import com.onlineretailstore.app.util.Constant;

@RestController
@RequestMapping("/retail-store")
public class BillController {
	
	@Autowired
	private BillService billService;

	@GetMapping(value = "/bills")
	public ResponseEntity<RestGenericTemplate<Bill>> getAllBills() {
		final RestGenericTemplate<Bill> response = new RestGenericTemplate<>();
		List<Bill> bill = billService.getAllBills();
		if(!CollectionUtils.isEmpty(bill)) {
			response.setData(bill);
			response.setTotalRecords(bill.size());
			response.setResponseCode(Constant.SUCCESS_CODE);
			response.setMessage("success");
		} 
		return new ResponseEntity<RestGenericTemplate<Bill>>(response, HttpStatus.OK);
	}

	@GetMapping(value = "/bills/{id}")
	public ResponseEntity<RestGenericTemplateSingle<Bill>> getBillById(@PathVariable Long id) {
		final RestGenericTemplateSingle<Bill> response = new RestGenericTemplateSingle<>();
		Bill bill = billService.getBillById(id);
		if(bill!=null) {
			response.setData(bill);
			response.setResponseCode(Constant.SUCCESS_CODE);
			response.setMessage("success");
		}
		return new ResponseEntity<RestGenericTemplateSingle<Bill>>(response, HttpStatus.OK);
	}
	
	@PostMapping(value = "/bills")
	public ResponseEntity<RestGenericTemplateSingle<Bill>> createBill() {
		final RestGenericTemplateSingle<Bill> result = new RestGenericTemplateSingle<Bill>();
		Bill bill = billService.createBill(new Bill(0.0, 0));
		if(bill != null) {
			result.setData(bill);
			result.setResponseCode(Constant.SUCCESS_CODE);
			result.setMessage("success");
		}
		return new ResponseEntity<RestGenericTemplateSingle<Bill>>(result, HttpStatus.OK);
	}

	@DeleteMapping(value = "/bills/{id}")
	public ResponseEntity<RestGenericTemplateSingle<String>> deleteBill(@PathVariable Long id) {
		final RestGenericTemplateSingle<String> stringResult = new RestGenericTemplateSingle<String>();
		stringResult.setMessage(billService.deleteBill(id));
		return new ResponseEntity<RestGenericTemplateSingle<String>>(stringResult, HttpStatus.OK);
	}

	@PutMapping(value = "/bills/{id}")
	public ResponseEntity<RestGenericTemplateSingle<Bill>> updateBill(@RequestBody BillInfo billInfo, @PathVariable Long id) {
		final RestGenericTemplateSingle<Bill> result = new RestGenericTemplateSingle<Bill>();
		Bill bill = billService.updateBill(billInfo, id);
		if(bill != null) {
			result.setData(bill);
			result.setResponseCode(Constant.SUCCESS_CODE);
			result.setMessage("success");
		}
		return new ResponseEntity<RestGenericTemplateSingle<Bill>>(result, HttpStatus.OK);
	}

}

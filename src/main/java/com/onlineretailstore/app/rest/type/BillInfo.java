package com.onlineretailstore.app.rest.type;

import java.util.List;

public class BillInfo {
	
	private List<ProductInfoForBill> productsToBeAdded;
	private List<ProductInfoForBill> productsToBeRemoved;
	/**
	 * @return the productsToBeAdded
	 */
	public List<ProductInfoForBill> getProductsToBeAdded() {
		return productsToBeAdded;
	}
	/**
	 * @param productsToBeAdded the productsToBeAdded to set
	 */
	public void setProductsToBeAdded(List<ProductInfoForBill> productsToBeAdded) {
		this.productsToBeAdded = productsToBeAdded;
	}
	/**
	 * @return the productsToBeRemoved
	 */
	public List<ProductInfoForBill> getProductsToBeRemoved() {
		return productsToBeRemoved;
	}
	/**
	 * @param productsToBeRemoved the productsToBeRemoved to set
	 */
	public void setProductsToBeRemoved(List<ProductInfoForBill> productsToBeRemoved) {
		this.productsToBeRemoved = productsToBeRemoved;
	}

}

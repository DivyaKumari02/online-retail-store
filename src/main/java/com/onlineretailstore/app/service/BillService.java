package com.onlineretailstore.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlineretailstore.app.ErrorHandler.CustomException;
import com.onlineretailstore.app.dto.Bill;
import com.onlineretailstore.app.dto.Item;
import com.onlineretailstore.app.dto.Product;
import com.onlineretailstore.app.repository.BillRepository;
import com.onlineretailstore.app.repository.ItemRepository;
import com.onlineretailstore.app.repository.ProductRepository;
import com.onlineretailstore.app.rest.type.BillInfo;
import com.onlineretailstore.app.rest.type.ProductInfoForBill;
import com.onlineretailstore.app.util.Constant;
import com.onlineretailstore.app.util.ProductCategory;

@Service
public class BillService {
	
	@Autowired
	BillRepository billRepository;
	
	@Autowired
	ItemRepository itemRepository;
	
	@Autowired
	ProductRepository productRepository;

	/**
	 * @param bill
	 * @return
	 */
	public Bill createBill(Bill bill) {
		Bill bill1 = billRepository.save(bill);
		return bill1;
	}
	
	/**
	 * @param id
	 * @return
	 */
	@Transactional
	public String deleteBill(Long id) {
		if(id != null) {
			Bill bill  = billRepository.findById(id);
			if(bill != null) {
				billRepository.deleteById(id);
				return "success";
			} else {
				return "bill not found";
			}
		}
		return "not found";
	}

	/**
	 * @return
	 */
	public List<Bill> getAllBills() {
		return (List<Bill>) billRepository.findAll();
	}

	/**
	 * @param id
	 * @return
	 */
	public Bill getBillById(Long id) {
		return billRepository.findById(id);
	}

	/**
	 * @param billInfo
	 * @param id
	 * @return
	 */
	public Bill updateBill(BillInfo billInfo, Long id) {
		if (null == billInfo) {
			throw new CustomException("There is no information to be updated for id " + id);
		}
		
		Bill bill  = billRepository.findById(id);
		
		if (bill == null) {
			throw new CustomException("Bill with id " + id + " not found");
		}
		
		if (null != billInfo.getProductsToBeAdded()) {
			List<ProductInfoForBill> prodToBeAdded = billInfo.getProductsToBeAdded();
			Iterator<ProductInfoForBill> prodToBeAddedIter = prodToBeAdded.iterator();
			while (prodToBeAddedIter.hasNext()) {
				ProductInfoForBill pInfo = prodToBeAddedIter.next();
				addProductToBill(id, pInfo.getBarCodeId(), pInfo.getQuantity(), bill);
			}
		}

		if (null != billInfo.getProductsToBeRemoved()) {
			List<ProductInfoForBill> prodToBeRemoved = billInfo.getProductsToBeRemoved();
			Iterator<ProductInfoForBill> prodToBeRemovedIter = prodToBeRemoved.iterator();
			while (prodToBeRemovedIter.hasNext()) {
				ProductInfoForBill pInfo = prodToBeRemovedIter.next();
				removeProductFromBill(id, pInfo.getBarCodeId(), pInfo.getQuantity(), bill);
			}
		}

		computeTotalValues(bill);
		return bill;
	}

	/**
	 * @param bill
	 */
	private void computeTotalValues(Bill bill) {
		int noOfItems = 0;
		double totalValue = 0;
		double totalCost = 0;

		if (null != bill.getItem()) {
			List<Item> item = bill.getItem();
			Iterator<Item> itemsIter = item.iterator();
			while (itemsIter.hasNext()) {
				Item li = itemsIter.next();
				double saleValue = computeValueForItem(li.getQuantity(), li.getProduct().getProductCategory(),
						li.getProduct().getPrice());
				totalValue += saleValue;
				totalCost += li.getQuantity() * li.getProduct().getPrice();
				noOfItems++;
			}
		}
		bill.setNoOfItems(noOfItems);
		bill.setTotalValue(totalValue);
		bill.setTotalCost(totalCost);
		bill.setTotalTax(totalValue - totalCost);
		billRepository.save(bill);
		
	}

	/**
	 * @param quantity
	 * @param productCategory
	 * @param price
	 * @return
	 */
	private double computeValueForItem(long quantity, ProductCategory productCategory, double price) {
		double saleValue = 0;
		if (productCategory.equals(ProductCategory.A)) {
			saleValue = quantity * price * 1.1; // 10% levy

		} else if (productCategory.equals(ProductCategory.B)) {
			saleValue = quantity * price * 1.2; // 20% levy

		} else if (productCategory.equals(ProductCategory.C)) {
			saleValue = quantity * price; // 0% levy
		}
		return saleValue;
	}

	/**
	 * @param id
	 * @param barCodeId
	 * @param quantity
	 * @param bill
	 * @return
	 */
	private Bill removeProductFromBill(Long id, String barCodeId, int quantity, Bill bill) {
		List<Item> currentItems = bill.getItem();
		// check if the product exists in product master
		verifyIfProductExists(barCodeId);

		if (currentItems != null && !currentItems.isEmpty()) {
			Item item = getItemWithBarCodeId(barCodeId, currentItems);
			if (null == item) {
				throw new CustomException(
						Constant.INPUT_ERROR_MESSAGE+"Product does not exist in current list of products. Cannot remove product with BarCode ID "
								+ barCodeId);
			}
			currentItems.remove(item);
			if(item.getQuantity() > quantity) {
				item.setQuantity(item.getQuantity()-quantity);
				currentItems.add(item);
			}
			bill.setItem(currentItems);
		} else {
			throw new CustomException(
					Constant.INPUT_ERROR_MESSAGE+"There are no items currently in the Bill. Cannot remove product with BarCode ID "
							+ barCodeId);
		}
		return bill;
	}

	/**
	 * @param id
	 * @param barCodeId
	 * @param quantity
	 * @param bill
	 * @return
	 */
	private Bill addProductToBill(Long id, String barCodeId, int quantity, Bill bill) {
		Product selectedProduct1 = verifyIfProductExists(barCodeId);

		Item l1 = new Item(selectedProduct1, quantity);
		itemRepository.save(l1);

		List<Item> currentItems = bill.getItem();
		if (currentItems != null) { 
			Item existingLi = getItemWithBarCodeId(barCodeId, currentItems);
			if (existingLi == null) {

				bill.getItem().add(l1); 
			} else {
				long newQty = existingLi.getQuantity() + quantity;
				existingLi.setQuantity(newQty); 
			}

		} else {
			currentItems = new ArrayList<>();
			currentItems.add(l1);
			bill.setItem(currentItems);
		}
		return bill;
	}

	/**
	 * @param barCodeId
	 * @param currentItems
	 * @return
	 */
	private Item getItemWithBarCodeId(String barCodeId, List<Item> currentItems) {
		for (int i = 0; i < currentItems.size(); i++) {
			Item li = currentItems.get(i);
			if (barCodeId.equals(li.getProduct().getBarCodeId())) {
				return li;
			}
		}
		return null;
	}

	/**
	 * @param barCodeId
	 * @return
	 */
	private Product verifyIfProductExists(String barCodeId) {
			List<Product> productsByBarCodeID = productRepository.findByBarCodeId(barCodeId);
			if (null == productsByBarCodeID || productsByBarCodeID.isEmpty()) {
				throw new CustomException(
						Constant.INPUT_ERROR_MESSAGE+"BarCode ID " + barCodeId + " does not exist in Product Master");
			}
			return productsByBarCodeID.get(0);
		}

}

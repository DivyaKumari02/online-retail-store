package com.onlineretailstore.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlineretailstore.app.dto.Product;
import com.onlineretailstore.app.repository.ProductRepository;
import com.onlineretailstore.app.rest.type.ProductData;

@Service
public class ProductService {
	
	@Autowired
	ProductRepository productRepository;

	/**
	 * @return
	 */
	public List<Product> getAllProduct() {
		return (List<Product>) productRepository.findAll();
	}
	
	/**
	 * @param id
	 * @return
	 */
	public Product getProductById(Long id) {
		return productRepository.findById(id);
	}

	/**
	 * @param productData
	 * @return
	 */
	public Product createProduct(ProductData productData) {
		if(productData != null) {
			Product product = new Product();
			BeanUtils.copyProperties(productData, product);
			return productRepository.save(product);
		}
		return null;
	}

	/**
	 * @param id
	 * @param productData
	 * @return
	 */
	public Product updateProduct(Long id, ProductData productData) {
		Product product  = productRepository.findById(id);
		if(product != null) {
			BeanUtils.copyProperties(productData, product);
			return productRepository.save(product);
		}
		return null;
	}
	
	/**
	 * @param id
	 * @return
	 */
	@Transactional
	public String deleteProduct(Long id) {
		if(id != null) {
			Product product  = productRepository.findById(id);
			if(product != null) {
				productRepository.deleteById(id);
				return "success";
			} else {
				return "product not found";
			}
		}
		return "not found";
	}

}

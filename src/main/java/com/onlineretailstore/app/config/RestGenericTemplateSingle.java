package com.onlineretailstore.app.config;

// TODO: Auto-generated Javadoc

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * The Class RestGenericTemplate.
 *
 * @param <T> the generic type
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestGenericTemplateSingle<T> {

	private T data;

	private String responseCode;

	private String message;

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	

}

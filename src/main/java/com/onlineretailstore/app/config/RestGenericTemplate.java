package com.onlineretailstore.app.config;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class RestGenericTemplate.
 *
 * @param <T> the generic type
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestGenericTemplate<T> {

    private List<T> data;

    private int totalRecords;

    private String responseCode;
    
    private String message;

	/**
	 * @return the data
	 */
	public List<T> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(List<T> data) {
		this.data = data;
	}

	/**
	 * @return the totalRecords
	 */
	public int getTotalRecords() {
		return totalRecords;
	}

	/**
	 * @param totalRecords the totalRecords to set
	 */
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

   
}

package com.onlineretailstore.app.util;

public class Constant {
	
	public static final String SUCCESS_CODE = "200";
	
	public static final String ERROR_CODE = "404";
	
	public static final String INPUT_ERROR_MESSAGE = "Problem with input data : ";

}

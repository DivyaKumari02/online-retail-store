package com.onlineretailstore.app.repository;

import org.springframework.data.repository.CrudRepository;

import com.onlineretailstore.app.dto.Bill;

public interface BillRepository extends CrudRepository<Bill, String>{

	Bill findById(Long id);

	void deleteById(Long id);

}

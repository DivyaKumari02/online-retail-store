package com.onlineretailstore.app.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.onlineretailstore.app.dto.Product;

public interface ProductRepository extends CrudRepository<Product, String>{

	Product findById(Long id);

	void deleteById(Long id);

	List<Product> findByBarCodeId(String barCodeId);
}

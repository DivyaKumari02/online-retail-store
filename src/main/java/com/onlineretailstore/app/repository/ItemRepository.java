package com.onlineretailstore.app.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.onlineretailstore.app.dto.Item;

public interface ItemRepository extends CrudRepository<Item, Long> {
	
	public List<Item> findByProduct_id(long prodId);

}

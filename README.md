# Online Retail Store Application: 

>**This project consists of Spring boot Restful API to checkout counter for an online retail store that scan products and generates an itemized bill.**

## Description
> It provides services for managing products and orders.  Products can be configured with rate and category (A,B or C).
> Sales tax is applied based on the category of the product:
```
 Category A - 10%
 Category B - 20%
 Category C- 0%
```

## REST ENDPOINTS
User can get/add/update/delete products and orders using the REST endpoints.Below is overview of REST end points:

## Products
```
  GET /products - fetches list of all product data
  GET /products/{id} - fetch a specific product
  POST /products - Creates a new product based on request JSON
  PUT /products/{id} - Updates product data based on request JSON
  DELETE /products/{id} - Delete an existing product if it is not associated with a bill.
```


## Bills
```
  GET /bills - fetches all bill data
  GET /bills/{id} - fetches bill of a particular id
  POST /bills - creates a bill Id. Client has to use this bill Id while adding and removing products
  PUT /bills/{id} - Updates bill data. Client can add or remove products to bill sending a JSON request.
  DELETE /bills/{id} - Delete bill from the system.
```

# Tools Used

This program and instructions have been tested on following versions on Windows.
```
  Apache Maven 3.5.3 
  Java version: 1.8.0_201
  git version 2.21.0.windows.1
  sts version 4.1.1.RELEASE
  mysql version 8.0
```


# How to run the application locally?

Pre-requisites to run application are Java, Maven and Git. 
```
  Installation instructions for Maven are available at https://maven.apache.org/install.html
  Java can be installed from http://www.oracle.com/technetwork/java/javase/downloads/index.html
  Latest Git version can be installed from https://git-scm.com/downloads
  STS IDE can be installed from https://spring.io/tools3/sts/all
```

Steps to build and run locally:
* Open commandline
* Create a new directory called "OnlineRetailStoreApplication" 
* Clone repository using following command=>   git clone https://DivyaKumari02@bitbucket.org/DivyaKumari02/online-retail-store.git.
* Build the executable jar using maven=> mvn package  

This application uses mysql database. 

For more details on API schema, Please check the Postman file "OnlineRetailStoreApplication.postman_collection.json"
